import setuptools

setuptools.setup(
    name='factdata',
    version='2.0.0',
    description='Wrapper for random fact API.',
    packages=setuptools.find_packages(),
    author='Cakebot Team',
    author_email='me@rdil.rocks',
    url='https://github.com/cakebotpro/factdata'
)
