import urllib
import json


class FactImp:
    def __init__(self):
        urllib.request.urlcleanup()
        self.req = urllib.request.Request(
            "https://uselessfacts.jsph.pl/random.json?language=en"
        )
        self.obj = json.loads(urllib.request.urlopen(self.req).read())

    def fact(self):
        return self.obj['text']
